using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.RabbitMqTransport;

namespace Cloudburst.EventBus.Abstractions {
	public abstract class EventBusConsumer<TMessage> : IEventBusConsumer<TMessage> where TMessage : class {
		public virtual void ReceiveEndpointConfiguration(IRabbitMqReceiveEndpointConfigurator configurator, IServiceProvider provider) { }

		public abstract Task Consume(ConsumeContext<TMessage> context);
	}
}

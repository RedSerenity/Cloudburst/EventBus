using System;
using MassTransit;
using MassTransit.RabbitMqTransport;

namespace Cloudburst.EventBus.Abstractions {
	public interface IEventBusConsumer {
		void ReceiveEndpointConfiguration(IRabbitMqReceiveEndpointConfigurator configurator, IServiceProvider provider);
	}

	public interface IEventBusConsumer<in TMessage> : IEventBusConsumer, IConsumer<TMessage> where TMessage : class { }
}

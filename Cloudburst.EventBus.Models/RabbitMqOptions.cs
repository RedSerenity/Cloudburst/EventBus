using System;

namespace Cloudburst.EventBus.Models {
	public class RabbitMqOptions {
		public string Host { get; set; } = "localhost";
		public string VirtualHost { get; set; } = "/";
		public string Username { get; set; } = String.Empty;
		public string Password { get; set; } = String.Empty;
		public bool UseHealthChecks { get; set; } = true;
	}
}

﻿using System;
using System.Linq;
using Cloudburst.Configuration;
using Cloudburst.EventBus.Abstractions;
using Cloudburst.EventBus.Models;
using GreenPipes;
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MassTransit.Metadata;
using MassTransit.RabbitMqTransport;
using MassTransit.Util;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cloudburst.EventBus {
	// TODO: This needs lots of work to use more features and be more configurable,
	// TODO: but this basic implementation will work for now.
	public static class EventBusExtensions {

		public static IHostBuilder UseCloudburstEventBus(this IHostBuilder builder) {
			builder.ConfigureServices((buildContext, services) => {
				services.UseCloudburstEventBus(buildContext.Configuration);
			});
			return builder;
		}

		public static IServiceCollection UseCloudburstEventBus(this IServiceCollection services, IConfiguration configuration) {
			var eventBusOptions = services.ConfigureMonitor<EventBusOptions>(configuration);

			if (eventBusOptions.CurrentValue is null) {
				Console.WriteLine("EventBus configuration is missing. EventBus will not be started.");
				return services;
			}

			var rabbitMqOptions = eventBusOptions.CurrentValue.RabbitMq;

			services.AddMassTransit(serviceConfigurator => {
				// Load all IConsumer Consumers
				serviceConfigurator.LoadAllConsumers();

				// Find and register receive endpoints with IEventBusConsumers
				serviceConfigurator.AddBus(provider =>
					Bus.Factory.CreateUsingRabbitMq(busConfigurator => {
						busConfigurator.Host(
							rabbitMqOptions.Host,
							rabbitMqOptions.VirtualHost,
							hostConfig => {
								if (!String.IsNullOrEmpty(rabbitMqOptions.Username)) {
									hostConfig.Username(rabbitMqOptions.Username);
								}

								if (!String.IsNullOrEmpty(rabbitMqOptions.Password)) {
									hostConfig.Password(rabbitMqOptions.Password);
								}
							});

						if (rabbitMqOptions.UseHealthChecks) {
							busConfigurator.UseHealthCheck(provider);
						}

						busConfigurator.RegisterReceiveEndpointsToConsumers(provider);
					})
				);
			});

			services.AddMassTransitHostedService();

			var busControl = services.BuildServiceProvider().GetService<IBusControl>();
			busControl.StartAsync();

			return services;
		}

		private static void LoadAllConsumers(this IServiceCollectionConfigurator configurator) {
			TypeSet types = AssemblyTypeCache.FindTypes(
					AppDomain.CurrentDomain.GetAssemblies(),
					TypeMetadataCache.IsConsumerOrDefinition
				).GetAwaiter().GetResult();

			configurator.AddConsumers(types.FindTypes(TypeClassification.Concrete | TypeClassification.Closed).ToArray());
		}

		private static bool IsEventBustConsumerInterface(Type type) =>
			typeof(IEventBusConsumer).IsAssignableFrom(type) &&
			!type.IsInterface && !type.IsAbstract;

		private static Type[] GetEventBusConsumers() =>
			AssemblyTypeCache.FindTypes(
					AppDomain.CurrentDomain.GetAssemblies(),
					IsEventBustConsumerInterface
				).GetAwaiter().GetResult()
				.FindTypes(TypeClassification.Concrete | TypeClassification.Closed)
				.ToArray();

		private static void RegisterReceiveEndpointsToConsumers(this IRabbitMqBusFactoryConfigurator configurator, IServiceProvider provider) {
			foreach (var consumer in GetEventBusConsumers()) {
				configurator.ReceiveEndpoint(consumer.FullName, ep => {
					ep.PrefetchCount = 16;
					ep.UseMessageRetry(r => r.Interval(2, 100));
					ep.ConfigureConsumer(provider, consumer);

					Console.WriteLine($"Registering Consumer {consumer.FullName}");

					var instance = (IEventBusConsumer) Activator.CreateInstance(consumer);
					if (instance is null) {
						throw new Exception($"Could not create instance of {consumer.FullName}.");
					}

					instance.ReceiveEndpointConfiguration(ep, provider);
				});
			}
		}

	}
}
